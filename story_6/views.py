from django.shortcuts import render, HttpResponse, redirect, get_object_or_404, reverse
from .models import Peserta, Kegiatan
from django.contrib import messages
from .forms import PesertaForm, KegiatanForm

def index(request):
    context = {
        "kegiatan_items" : Kegiatan.objects.all(),
        "peserta_form" : PesertaForm,
        "is_index" : True
    }
    return render(request,"story_6/index.html",context)

def add_peserta(request,id):
    if request.method == "POST":
        kegiatan = Kegiatan.objects.get(id=id)
        nama_peserta_baru = request.POST["nama"]

        peserta_baru, status_baru = Peserta.objects.get_or_create(nama=nama_peserta_baru)
        if status_baru:
            peserta_baru.save()
            kegiatan.peserta.add(peserta_baru) # Add peserta baru yang tidak pernah ada.
            messages.add_message(request, messages.SUCCESS, "{} was successfully added to {}".format(nama_peserta_baru,kegiatan.nama))
        else:
            try:
                kegiatan.peserta.get(nama=nama_peserta_baru) # Jangan add peserta yang sudah di kegiatan.
                messages.add_message(request, messages.WARNING, "{} is already a participant in {}".format(nama_peserta_baru,kegiatan.nama))
            except:
                kegiatan.peserta.add(peserta_baru) # Add peserta baru yang ada di database tapi tidak di kegiatan.
                messages.add_message(request, messages.SUCCESS, "{} was successfully added to {}".format(nama_peserta_baru,kegiatan.nama))
    return redirect(reverse("story_6:detail_kegiatan",args=[kegiatan.nama.replace(" ","-")]))

def detail_peserta(request,nama):
    nama = nama.replace("-"," ")
    peserta = get_object_or_404(Peserta,nama=nama)
    context = {
        "peserta" : peserta
    }
    return render(request,"story_6/peserta_detail.html",context)

def detail_kegiatan(request,nama):
    nama = nama.replace("-"," ")
    kegiatan = get_object_or_404(Kegiatan,nama=nama)
    context = {
        "kegiatan" : kegiatan,
        "peserta_form" : PesertaForm
    }
    return render(request,"story_6/kegiatan_detail.html",context)

def add_kegiatan(request):
    if request.method == "POST":
        kegiatan_form = KegiatanForm(request.POST)
        if kegiatan_form.is_valid():
            kegiatan_object = kegiatan_form.save(commit=False)
            slug = kegiatan_object.nama.replace(" ", "-")
            kegiatan_form.save()
        return redirect(reverse("story_6:detail_kegiatan",args=[slug]))

    context = {
        "kegiatan_form" : KegiatanForm,
        "is_add_kegiatan" : True
    }
    return render(request,"story_6/add_kegiatan.html",context)

def delete_peserta(request):
    if request.method == "POST":
        try:
            peserta = Peserta.objects.get(nama = request.POST["nama"])
            kegiatan = Kegiatan.objects.get(id = request.POST["kegiatan_id"])
        except:
            messages.add_message(request, messages.WARNING, "{} was not found on {} as a participant.".format(request.POST["nama"], request.POST["kegiatan_id"]))
            return redirect("story_6:home")
        kegiatan.peserta.remove(peserta)
        messages.add_message(request, messages.SUCCESS, "{} has been removed from {}.".format(peserta.nama, kegiatan.nama))
    return redirect("story_6:home")
    
