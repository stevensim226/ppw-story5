from django.test import TestCase, Client
from .models import Peserta, Kegiatan

from django.urls import reverse, resolve
from .views import index, add_peserta, detail_kegiatan, detail_peserta
from .models import Peserta, Kegiatan

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.index_url = reverse("story_6:home")
        self.detail_url = reverse("story_6:detail_kegiatan",args=["kegiatan-satu"])
        self.kegiatan = Kegiatan.objects.create(
            nama = "kegiatan satu"
        )
        self.existing_peserta = Peserta.objects.create(nama="existing peserta")
        self.add_peserta_url = reverse("story_6:add_peserta",args=[self.kegiatan.id])
        self.detail_peserta_url = reverse("story_6:detail_peserta",args=["existing peserta"])
        self.add_kegiatan_url = reverse("story_6:add_kegiatan")
        self.peserta_to_be_deleted = Peserta.objects.create(nama="delete_me")
        self.delete_peserta_url = reverse("story_6:delete_kegiatan")

    def test_index_GET(self):
        response = self.client.get(self.index_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,"story_6/index.html")
        self.assertContains(response,"Event Organizer")

    def test_detail_kegiatan_GET(self):
        response = self.client.get(self.detail_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,"story_6/kegiatan_detail.html")

    def test_detail_peserta_GET(self):
        response = self.client.get(self.detail_peserta_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,"story_6/peserta_detail.html")
        
    def test_add_peserta_POST_adds_nama(self):
        response = self.client.post(self.add_peserta_url, {
            "nama" : "test_nama"
        })
        self.assertEquals(response.status_code,302)
        self.assertEquals(self.kegiatan.peserta.all()[0].nama,"test_nama")

    def test_add_peserta_POST_name_on_database(self):
        response = self.client.post(self.add_peserta_url, {
            "nama" : "existing peserta"
        })
        self.assertEquals(response.status_code,302)
        self.assertEquals(self.kegiatan.peserta.all()[0].nama,"existing peserta")

    def test_add_peserta_POST_same_name(self):
        self.client.post(self.add_peserta_url, {
            "nama" : "peserta"
        })
        response = self.client.post(self.add_peserta_url, {
            "nama" : "peserta"
        })
        self.assertEquals(response.status_code,302)
        self.assertEquals(len(self.kegiatan.peserta.all()),1)

    def test_add_kegiatan_GET(self):
        response = self.client.get(self.add_kegiatan_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,"story_6/add_kegiatan.html")
    
    def test_add_kegiatan_POST_adds_kegiatan_and_redirects(self):
        response = self.client.post(self.add_kegiatan_url, {
            "nama" : "nama kegiatan"
        })
        self.assertEquals(response.status_code,302)

    def test_delete_peserta_POST_deletes_peserta(self):
        # add dulu pesertanya
        self.kegiatan.peserta.add(self.peserta_to_be_deleted)
        old_length = len(self.kegiatan.peserta.all())

        # lalu di delete
        response = self.client.post(self.delete_peserta_url, {
            "nama" : "delete_me",
            "kegiatan_id" : self.kegiatan.id
        })
        self.assertEquals(len(self.kegiatan.peserta.all()), old_length-1)
        self.assertEquals(response.status_code, 302)

    def test_delete_peserta_POST_peserta_not_found(self):
        response = self.client.post(self.delete_peserta_url, {
            "nama" : "not_found",
            "kegiatan_id" : self.kegiatan.id
        }, follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, "was not found")

class TestModels(TestCase):
    def setUp(self):
        self.peserta_test = Peserta.objects.create(
            nama = "peserta satu"
        )
        self.kegiatan_test = Kegiatan.objects.create(
            nama = "kegiatan satu"
        )
        self.kegiatan_test.peserta.add(self.peserta_test)

    def test_nama_peserta(self):
        self.assertEquals(str(self.peserta_test),"peserta satu")

    def test_nama_kegiatan(self):
        self.assertEquals(str(self.kegiatan_test),"kegiatan satu")

    def test_absolute_url_peserta(self):
        self.assertEquals(self.peserta_test.get_absolute_url(),"/story_6/profile/peserta-satu")