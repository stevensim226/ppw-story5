from django.contrib import admin
from .models import Peserta, Kegiatan

admin.site.register(Peserta)
admin.site.register(Kegiatan)