from django.db import models
from django.shortcuts import reverse

class Peserta(models.Model):
    nama = models.TextField(max_length=100, unique=True)

    def __str__(self):
        return self.nama
    
    def get_absolute_url(self):
        nama_link = self.nama.replace(" ","-")
        return reverse("story_6:detail_peserta",args=[nama_link])

class Kegiatan(models.Model):
    nama = models.TextField(max_length=100,unique=True)
    peserta = models.ManyToManyField(Peserta,blank=True)

    def __str__(self):
        return self.nama

    def get_absolute_url(self):
        nama_link = self.nama.replace(" ","-")
        return reverse("story_6:detail_kegiatan",args=[nama_link])