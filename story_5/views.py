from django.shortcuts import render, redirect
from .models import MataKuliah, TugasMataKuliah
from .forms import MatkulForm
from django.utils import timezone
from django.contrib import messages

def index(request):
    context = {
        "mata_kuliah_list" : MataKuliah.objects.all().order_by("-id"),
        "page" : "index"
    }
    return render(request,"story_5/index.html",context)

def add_matkul(request):
    if request.method == "POST":
        matkul_form = MatkulForm(request.POST)
        if matkul_form.is_valid():
            matkul_form.save()
            messages.add_message(request, messages.SUCCESS, "Mata Kuliah '{}' successfully added".format(request.POST["nama"]))
        return redirect("story_5:index")
    context = {
        "page" : "add_page"
    }
    return render(request,"story_5/add_matkul.html",context)

def matkul_detail(request,id):
    matkul = MataKuliah.objects.get(id=id)
    context = {
        "mata_kuliah" : matkul,
        "tugas_objects" : TugasMataKuliah.objects.filter(mata_kuliah=matkul).order_by("deadline"),
        "time_now" : timezone.now()
    }

    tugas_deadline = []
    for matkul in context["tugas_objects"]:
        diff = matkul.deadline - timezone.now()  
        tugas_deadline.append((matkul,diff.total_seconds())) #get detik perbedaan
    context["matkul_deadlines"] = tugas_deadline
    return render(request,"story_5/detail_matkul.html",context)

def delete_matkul(request,id):
    if request.method == "POST":
        mata_kuliah = MataKuliah.objects.get(id=id)
        mata_kuliah.delete()
        messages.add_message(request, messages.SUCCESS, "Mata Kuliah '{}' successfully deleted".format(mata_kuliah.nama))
    return redirect("story_5:index")