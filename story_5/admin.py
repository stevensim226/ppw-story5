from django.contrib import admin
from .models import MataKuliah, TugasMataKuliah

# Register your models here.
admin.site.register(MataKuliah)
admin.site.register(TugasMataKuliah)