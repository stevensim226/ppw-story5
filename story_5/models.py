from django.db import models
from django.shortcuts import reverse
from django.utils import timezone

semester_choices = [
    ("2019/2020 Gasal","2019/2020 Gasal"),
    ("2019/2020 Genap","2019/2020 Genap"),
    ("2020/2021 Gasal","2020/2021 Gasal"),
    ("2020/2021 Genap","2020/2021 Genap"),
    ("2021/2022 Gasal","2021/2022 Gasal"),
    ("2021/2022 Genap","2021/2022 Genap"),
    ("2022/2023 Gasal","2022/2023 Gasal"),
    ("2022/2023 Genap","2022/2023 Genap")
]

class MataKuliah(models.Model):
    nama = models.TextField(max_length = 50)
    dosen = models.TextField(max_length = 50)
    jumlah_sks = models.IntegerField()
    deskripsi = models.TextField(max_length = 500)
    semester = models.TextField(choices = semester_choices)
    ruangan = models.TextField(max_length = 50)

    def __str__(self):
        return "{} ({})".format(self.nama,self.dosen)

    def get_absolute_url(self):
        return reverse("story_5:matkul_detail",args=[self.id])

class TugasMataKuliah(models.Model):
    nama = models.TextField(max_length=50)
    deadline = models.DateTimeField()
    mata_kuliah = models.ForeignKey(MataKuliah,on_delete=models.CASCADE)

    def __str__(self):
        return "{} - {}".format(self.nama,self.mata_kuliah.nama)
