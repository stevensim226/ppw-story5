from django.shortcuts import redirect

def blank_page(request):
    return redirect("https://ppw-story4-steven.herokuapp.com/")