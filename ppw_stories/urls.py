from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path("story_5/",include("story_5.urls")),
    path("story_6/",include("story_6.urls")),
    path("",views.blank_page)
]
